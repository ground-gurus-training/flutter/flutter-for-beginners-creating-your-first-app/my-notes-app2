import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app2/controller/notes_controller.dart';

import '../../models/note.dart';

class NoteEdit extends StatelessWidget {
  final NotesController controller;
  final Note note;

  const NoteEdit({super.key, required this.controller, required this.note});

  @override
  Widget build(BuildContext context) {
    final titleController = TextEditingController();
    final noteController = TextEditingController();

    titleController.text = note.title;
    noteController.text = note.note;

    return SafeArea(
      child: Scaffold(
        body: Container(
          margin: const EdgeInsets.all(16.0),
          child: Form(
            child: Column(
              children: [
                TextFormField(
                  controller: titleController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Title',
                    labelText: 'Title',
                  ),
                ),
                const SizedBox(height: 20.0),
                TextFormField(
                  controller: noteController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Notes',
                    labelText: 'Notes',
                  ),
                  maxLines: 5,
                  maxLength: 255,
                ),
                const SizedBox(height: 20.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ElevatedButton.icon(
                      onPressed: () {
                        note.title = titleController.text;
                        note.note = noteController.text;
                        controller.updateNote(note);
                        Get.back();
                      },
                      icon: const Icon(Icons.save),
                      label: const Text('Save'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
