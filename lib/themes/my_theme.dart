import 'package:flutter/material.dart';

final myTheme = ThemeData(
  appBarTheme: const AppBarTheme(
    backgroundColor: Colors.amber,
    titleTextStyle: TextStyle(
      fontSize: 28,
      color: Colors.white,
    ),
  ),
  textTheme: const TextTheme(
    headline3: TextStyle(
      fontSize: 32,
      color: Colors.black,
    ),
    bodyText2: TextStyle(
      fontSize: 26,
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith(
        (states) => Colors.amber,
      ),
      textStyle: MaterialStateProperty.resolveWith(
        (states) => const TextStyle(fontSize: 24.0),
      ),
      padding: MaterialStateProperty.resolveWith(
        (states) => const EdgeInsets.all(16.0),
      ),
    ),
  ),
  floatingActionButtonTheme: const FloatingActionButtonThemeData(
    backgroundColor: Colors.amber,
    iconSize: 28,
  ),
  navigationBarTheme: const NavigationBarThemeData(
    indicatorColor: Colors.amber,
  ),
);
