import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:my_notes_app2/firebase_options.dart';
import 'package:my_notes_app2/themes/my_theme.dart';
import 'package:my_notes_app2/views/notes/note_edit.dart';

import 'controller/notes_controller.dart';
import 'models/note.dart';
import 'views/home/home_page.dart';
import 'views/about/about_page.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final Future<FirebaseApp> _fbApp = Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  final NotesController controller = Get.put(NotesController());
  int currentIndex = 0;
  bool hasNotesLoaded = false;

  loadNotes() async {
    if (hasNotesLoaded) return;

    final db = FirebaseFirestore.instance;
    final notes = db.collection('Notes');
    final snapshot = await notes.get();
    controller.loadNotes(snapshot.docs);
    hasNotesLoaded = true;
  }

  @override
  Widget build(BuildContext context) {
    final pages = [
      HomePage(
        controller: controller,
      ),
      const AboutPage(),
    ];

    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'My Notes App',
        theme: myTheme,
        home: FutureBuilder(
          future: _fbApp,
          builder: ((context, snapshot) {
            if (snapshot.hasError) {
              debugPrint('You have an error! ${snapshot.error.toString()}');
              return const Scaffold(
                body: Center(
                  child: Text(
                    'Something went wrong!',
                    style: TextStyle(fontSize: 32.0),
                  ),
                ),
              );
            } else if (snapshot.hasData) {
              loadNotes();

              return Scaffold(
                appBar: AppBar(
                  title: const Text('My Notes'),
                ),
                body: pages[currentIndex],
                floatingActionButton: FloatingActionButton(
                  onPressed: () {
                    Get.to(
                      () => NoteEdit(
                        controller: controller,
                        note: Note(),
                      ),
                    );
                  },
                  child: const Icon(Icons.add),
                ),
                bottomNavigationBar: NavigationBar(
                  selectedIndex: currentIndex,
                  onDestinationSelected: (value) => setState(() {
                    currentIndex = value;
                  }),
                  destinations: const [
                    NavigationDestination(
                      icon: Icon(Icons.house),
                      label: 'House',
                    ),
                    NavigationDestination(
                      icon: Icon(Icons.person),
                      label: 'About',
                    ),
                  ],
                ),
              );
            } else {
              return const CircularProgressIndicator();
            }
          }),
        ));
  }
}
